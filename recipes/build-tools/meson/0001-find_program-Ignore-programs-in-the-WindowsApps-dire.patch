From 901199361502eaecae6b709d20f835dcaf6dda8e Mon Sep 17 00:00:00 2001
From: Nirbheek Chauhan <nirbheek@centricular.com>
Date: Tue, 14 Jan 2020 13:46:59 +0530
Subject: [PATCH] find_program: Ignore programs in the WindowsApps directory

The latest Windows 10 release in May 2019 added zero-sized files that
act as stubs which when launched from cmd.exe spawn the Windows Store
to install those apps. This also includes python.exe and python3.exe:

https://devblogs.microsoft.com/python/python-in-the-windows-10-may-2019-update/

Unfortunately, `import('python').find_installation('python3')` will
then think that python3.exe is available on Windows. Or, worse, if the
user has a fresh installation of Windows 10 and then installs the
Python 3 using the official installer (not the Windows Store app), we
will *still* pickup this stub because it will be first in `PATH`.

Always remove the WindowsApps directory from `PATH` while searching.

First reported at https://gitlab.freedesktop.org/gstreamer/cerbero/issues/223
---
 mesonbuild/dependencies/base.py | 25 ++++++++++++++++++++++---
 1 file changed, 22 insertions(+), 3 deletions(-)

diff --git a/mesonbuild/dependencies/base.py b/mesonbuild/dependencies/base.py
index ab3f155..8ce614b 100644
--- a/mesonbuild/dependencies/base.py
+++ b/mesonbuild/dependencies/base.py
@@ -28,7 +28,7 @@ import platform
 import itertools
 import ctypes
 from enum import Enum
-from pathlib import PurePath
+from pathlib import PurePath, Path
 
 from .. import mlog
 from .. import mesonlib
@@ -1706,6 +1706,22 @@ class ExternalProgram:
         # Search for the command using the specified string!
         return ExternalProgram(command, silent=True)
 
+    @staticmethod
+    @functools.lru_cache(maxsize=None)
+    def _windows_sanitize_path(path: str) -> str:
+        # Ensure that we use USERPROFILE even when inside MSYS, MSYS2, Cygwin, etc.
+        if 'USERPROFILE' not in os.environ:
+            return path
+        # Ignore executables in the WindowsApps directory which are
+        # zero-sized wrappers that magically open the Windows Store to
+        # install the application.
+        appstore_dir = Path(os.environ['USERPROFILE']) / 'AppData' / 'Local' / 'Microsoft' / 'WindowsApps'
+        paths = []
+        for each in path.split(os.pathsep):
+            if Path(each) != appstore_dir:
+                paths.append(each)
+        return os.pathsep.join(paths)
+
     @staticmethod
     def _shebang_to_cmd(script):
         """
@@ -1812,7 +1828,7 @@ class ExternalProgram:
         # On Windows, interpreted scripts must have an extension otherwise they
         # cannot be found by a standard PATH search. So we do a custom search
         # where we manually search for a script with a shebang in PATH.
-        search_dirs = os.environ.get('PATH', '').split(';')
+        search_dirs = self._windows_sanitize_path(os.environ.get('PATH', '')).split(';')
         for search_dir in search_dirs:
             commands = self._search_dir(name, search_dir)
             if commands:
@@ -1828,7 +1844,10 @@ class ExternalProgram:
         if commands:
             return commands
         # Do a standard search in PATH
-        command = shutil.which(name)
+        path = os.environ.get('PATH', None)
+        if mesonlib.is_windows() and path:
+            path = self._windows_sanitize_path(path)
+        command = shutil.which(name, path=path)
         if mesonlib.is_windows():
             return self._search_windows_special_cases(name, command)
         # On UNIX-like platforms, shutil.which() is enough to find
-- 
2.24.1.windows.2

